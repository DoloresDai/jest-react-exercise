import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import 'babel-polyfill';

import OrderComponent from '../Order';

const setup = () => {
  const mock = jest.mock('axios'); // Mock axios模块
  const { getByTestId, getByLabelText } = render(<OrderComponent />);
  const status = getByTestId('status');
  const input = getByLabelText('number-input');
  const submit = getByLabelText('submit-button');
  return {
    mock,
    status,
    input,
    submit
  };
};

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  const { status, input, submit } = setup();
  fireEvent.change(input);
  expect(input).toBeDefined();

  await expect(status).toHaveTextContent('未完成');

  fireEvent.click(submit);

  await expect(status).toHaveTextContent('完成');

  // setup组件
  // Mock数据请求
  // 触发事件
  // 给出断言
  // --end->
});
